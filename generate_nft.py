import os
import json
import random
import re
from PIL import Image
import requests
import logging
import time
from collections import defaultdict
import math

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Define the paths to your layer directories dynamically
def get_layer_paths(base_dir='./layers'):
    return {folder: os.path.join(base_dir, folder) for folder in os.listdir(base_dir) if os.path.isdir(os.path.join(base_dir, folder))}

layer_paths = get_layer_paths()

# Path to store the API keys and configuration
API_KEYS_PATH = './pinata_api_keys.json'
CONFIG_PATH = './config.json'

# Function to parse file names and extract weights
def parse_file_weights(file_name):
    weight = re.findall(r'_([\d\.]+)\.png$', file_name)
    return float(weight[0]) if weight else 10

# Get all file names and weights for each layer
layer_files = {layer: sorted(os.listdir(path)) for layer, path in layer_paths.items()}
layer_weights = {
    layer: [parse_file_weights(file) for file in files] for layer, files in layer_files.items()
}

# Function to create a unique combination of layers ensuring no duplicate layers within a combination
def create_combination(existing_combinations, max_similarity):
    attempts = 0
    max_attempts = 10000

    while attempts < max_attempts:
        combination = {}
        used_files = set()
        for layer, files in layer_files.items():
            weights = layer_weights[layer]
            chosen_file = random.choices(files, weights=weights, k=1)[0]
            while chosen_file in used_files:
                chosen_file = random.choices(files, weights=weights, k=1)[0]
            combination[layer] = chosen_file
            used_files.add(chosen_file)

        combination_tuple = tuple((layer, combination[layer]) for layer in sorted(combination))

        # Check if the combination is sufficiently unique based on max_similarity
        if all(sum(1 for k, v in combination_tuple if dict(existing).get(k) != v) >= max_similarity for existing in existing_combinations):
            return combination

        attempts += 1

    raise ValueError("Unable to create a unique combination after 10000 attempts or more than the allowed similarity.")

# Function to generate an image from a combination of layers
def generate_image(combination, output_path):
    layers = [Image.open(os.path.join(layer_paths[layer], combination[layer])) for layer in combination]
    base = layers[0].copy()
    for layer in layers[1:]:
        base.paste(layer, (0, 0), layer)
    base.save(output_path)

# Function to upload an image to IPFS using Pinata
def upload_to_ipfs(image_path, api_key, secret_api_key):
    url = "https://api.pinata.cloud/pinning/pinFileToIPFS"
    headers = {
        "pinata_api_key": api_key,
        "pinata_secret_api_key": secret_api_key
    }
    with open(image_path, 'rb') as file:
        files = {'file': file}
        response = requests.post(url, files=files, headers=headers)
    if response.status_code == 200:
        ipfs_hash = response.json()['IpfsHash']
        return f"https://gateway.pinata.cloud/ipfs/{ipfs_hash}"
    else:
        raise Exception(f"Failed to upload to IPFS: {response.content}")

# Function to remove numeric prefixes from strings
def remove_numeric_prefix(string):
    return re.sub(r'^\d+', '', string)

# Function to remove numeric suffixes from strings
def remove_numeric_suffix(string):
    return re.sub(r'_\d+$', '', string)

# Function to process the file name and remove numeric prefixes                            
def process_file_name(file_name):
    base_name = os.path.splitext(file_name)[0]  # Get the base name without extension
    base_name = re.sub(r'^\d+_', '', base_name)  # Remove numeric prefix followed by underscore
    base_name = re.sub(r'\d+$', '', base_name)  # Remove numeric suffix
    return base_name

# Function to generate CIP-68 metadata
def generate_metadata(combination, project_name, project_desc, token_id, policy_id, ipfs_url):
    asset_name = f'{project_name}{token_id}'
    attributes = {remove_numeric_prefix(re.sub(r'^\d+_', '', layer)): process_file_name(combination[layer]).replace("_", " ") for layer in combination}
    
    # Construct the IPFS URI using ipfs:// protocol
    ipfs_hash = ipfs_url.split('/')[-1]  # Extract the hash from the full Pinata URL
    ipfs_uri = f'ipfs://{ipfs_hash}'
    
    metadata = {
        "721": {
            policy_id: {
                asset_name: {
                    "name": asset_name,
                    "description": project_desc,
                    "image": ipfs_uri,
                    "mediaType": "image/png",
                    **attributes  # Add attributes under "attributes"
                }
            }
        }
    }
    return metadata

# Function to save API keys to a file
def save_api_keys(api_key, secret_api_key):
    with open(API_KEYS_PATH, 'w') as f:
        json.dump({'pinata_api_key': api_key, 'pinata_secret_api_key': secret_api_key}, f)

# Function to load API keys from a file
def load_api_keys():
    if os.path.exists(API_KEYS_PATH):
        with open(API_KEYS_PATH, 'r') as f:
            return json.load(f)
    return None

# Function to estimate possible unique combinations considering max_similarity
def estimate_possible_combinations(layer_files, max_similarity):
    num_layers = len(layer_files)
    file_counts = [len(files) for files in layer_files.values()]
    
    # Total combinations without constraints
    total_combinations = math.prod(file_counts)
    
    # Approximate unique combinations considering max_similarity
    reduction_factor = ((num_layers - max_similarity) / num_layers) ** num_layers
    unique_combinations = int(total_combinations * reduction_factor)
    
    return unique_combinations

# Function to save the configuration to a file
def save_config(config):
    with open(CONFIG_PATH, 'w') as f:
        json.dump(config, f)

# Function to load the configuration from a file
def load_config():
    if os.path.exists(CONFIG_PATH):
        with open(CONFIG_PATH, 'r') as f:
            return json.load(f)
    return None

# Function to clear the output directory
def clear_output_dir(output_dir):
    if os.path.exists(output_dir):
        for file in os.listdir(output_dir):
            file_path = os.path.join(output_dir, file)
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                os.rmdir(file_path)
    else:
        os.makedirs(output_dir)

# Main function to run the NFT generation process
def main():
    # Prompt user for input
    print("Welcome to the Rage Republic NFT Generation Script!")
    config = load_config()
    
    if config:
        use_stored_config = input("Do you want to use stored configuration? (yes/no): ").strip().lower()
    else:
        use_stored_config = 'no'
        
    if use_stored_config == 'yes':
        if config:
            project_name = config['project_name']
            project_desc = config['project_desc']
            num_nfts = config['num_nfts']
            policy_id = config['policy_id']
            max_similarity = config['max_similarity']
            api_keys_choice = config['api_keys_choice']
            api_key = config['api_key']
            if api_key != 'skip':
                secret_api_key = config['secret_api_key']
            else:
                secret_api_key = 'skip'
            output_dir = config['output_dir']
            start_number = config['start_number']
        else:
            project_name = input("Name of your NFT Project: ").replace(" ", "")
            project_desc = input("Enter the project description: ")
            num_nfts = int(input("Enter the number of NFTs to generate: "))
            policy_id = input("Enter the Cardano policy ID: ")
            max_similarity = int(input("Enter the maximum number of layers that can be the same between combinations: "))
            api_keys_choice = input("Do you want to use locally stored API keys? (yes/no): ").strip().lower()
            api_key = input("Enter your Pinata API Key (type 'skip' if you'd like to omit this part): ")
            if api_key != 'skip':
                secret_api_key = input("Enter your Pinata secret API Key (type 'skip' if you wish to omit): ")
            else:
                secret_api_key = 'skip'
            output_dir = input("Enter the output directory: ")
            start_number = int(input("Enter the starting value of the sequence number: "))
            save_config({
                'project_name': project_name,
                'project_desc': project_desc,
                'num_nfts': num_nfts,
                'policy_id': policy_id,
                'max_similarity': max_similarity,
                'api_keys_choice': api_keys_choice,
                'api_key': api_key,
                'secret_api_key': secret_api_key,
                'output_dir': output_dir,
                'start_number': start_number
            })
    else:
        project_name = input("Name of your NFT Project: ").replace(" ", "")
        project_desc = input("Enter the project description: ")
        num_nfts = int(input("Enter the number of NFTs to generate: "))
        policy_id = input("Enter the Cardano policy ID: ")
        max_similarity = int(input("Enter the maximum number of layers that can be the same between combinations: "))
        #api_keys_choice = input("Do you want to use locally stored API keys? (yes/no): ").strip().lower()
        api_keys = load_api_keys()
        if api_keys:
            api_keys_choice = input("Do you want to use locally stored API keys? (yes/no): ").strip().lower()
        else: 
            api_keys_choice = 'no'

        if api_keys_choice == 'yes':
            api_keys = load_api_keys()
            if api_keys:
                api_key = api_keys['pinata_api_key']
                secret_api_key = api_keys['pinata_secret_api_key']
            else:
                print("No API keys found locally. Please provide them now.")
                api_key = input("Enter your Pinata API Key: ")
                if api_key != 'skip':
                    secret_api_key = input("Enter your Pinata secret API Key: ")
                    store_keys = input("Do you want to store these API keys for future use? (yes/no): ").strip().lower()
                    if store_keys == 'yes':
                        save_api_keys(api_key, secret_api_key)
                else:
                    secret_api_key = 'skip'
                
        else:
            api_key = input("Enter your Pinata API Key (type 'skip' if you'd like to omit this part): ")
            if api_key != 'skip':
                secret_api_key = input("Enter your Pinata secret API Key (type 'skip' if you wish to omit): ")
                store_keys = input("Do you want to store these API keys for future use? (yes/no): ").strip().lower()
                if store_keys == 'yes':
                    save_api_keys(api_key, secret_api_key)
            else:
                secret_api_key = 'skip'

        output_dir = input("Enter the output directory: ")
        start_number = int(input("Enter the starting value of the sequence number: "))
        
        save_config({
            'project_name': project_name,
            'project_desc': project_desc,
            'num_nfts': num_nfts,
            'policy_id': policy_id,
            'max_similarity': max_similarity,
            'api_keys_choice': api_keys_choice,
            'api_key': api_key,
            'secret_api_key': secret_api_key,
            'output_dir': output_dir,
            'start_number': start_number
        })
    # Clear the output directory
    clear_output_dir(output_dir)

    # Initialize variables
    existing_combinations = []
    ipfs_urls = []
    total_combinations = estimate_possible_combinations(layer_files, max_similarity)
    logging.info(f"Estimated possible unique combinations: {total_combinations}")
    # Ensure the number of NFTs requested is feasible
    if num_nfts > total_combinations:
        print(f"Based upon the estimated number of unique combinations estimated to exist (taking in to account maximum similarity), it may not be possible to generate {num_nfts} but let's give it a go...")

    # Start generating NFTs
    for token_id in range(start_number, start_number + num_nfts):
        try:
            # Create a unique combination
            combination = create_combination(existing_combinations, max_similarity)

            # Generate the image
            output_path = os.path.join(output_dir, f'{project_name}{token_id}.png')
            generate_image(combination, output_path)

            # Upload to IPFS if API keys are provided
            if api_key != 'skip' and secret_api_key != 'skip':
                ipfs_url = upload_to_ipfs(output_path, api_key, secret_api_key)
                ipfs_urls.append(ipfs_url)
            else:
                ipfs_urls.append('')

            # Generate and save metadata
            metadata = generate_metadata(combination, project_name, project_desc, token_id, policy_id, ipfs_urls[-1])
            metadata_path = os.path.join(output_dir, f'{project_name}{token_id}.json')
            with open(metadata_path, 'w') as f:
                json.dump(metadata, f, indent=4)

            # Append the combination to existing combinations
            existing_combinations.append(tuple((layer, combination[layer]) for layer in sorted(combination)))

            logging.info(f'Generated NFT {token_id}/{num_nfts}')

        except Exception as e:
            logging.error(f"Error generating NFT {token_id}: {str(e)}")

if __name__ == "__main__":
    main()
