import os
import json
from tkinter import Tk, filedialog

# Function to update policyID in all JSON metadata files in a directory
def update_policy_id(directory, original_policy_id, new_policy_id):
    for filename in os.listdir(directory):
        if filename.endswith('.json'):
            file_path = os.path.join(directory, filename)
            with open(file_path, 'r') as f:
                metadata = json.load(f)

            # Update policyID throughout the metadata
            for token_data in metadata.values():
                if original_policy_id in token_data:
                    token_data[new_policy_id] = token_data.pop(original_policy_id)
                    break  # Exit after replacing the first instance

            # Write updated metadata back to the file
            with open(file_path, 'w') as f:
                json.dump(metadata, f, indent=4)

# Function to open a file dialog to select the directory containing the JSON metadata files
def select_directory():
    root = Tk()
    root.withdraw()  # Hide the root window
    root.attributes('-topmost', True)  # Bring the file dialog to the front
    directory = filedialog.askdirectory(title="Select the directory containing the JSON metadata files")
    root.destroy()
    return directory

def main():
    while True:
        directory = select_directory()
        if not directory:
            print("No directory selected. Please select a valid directory.")
            continue
        if not os.path.isdir(directory):
            print(f"Error: '{directory}' is not a valid directory.")
            continue
        
        json_files = [f for f in os.listdir(directory) if f.endswith('.json')]
        if not json_files:
            print(f"No JSON files found in '{directory}'. Please select a different directory.")
        else:
            break

    original_policy_id = input("Enter the original Cardano policy ID used in the metadata files: ")
    new_policy_id = input("Enter the new Cardano policy ID: ")
    update_policy_id(directory, original_policy_id, new_policy_id)
    print(f"Updated first occurrence of policyID from {original_policy_id} to {new_policy_id} in all JSON metadata files in {directory}.")

if __name__ == "__main__":
    main()
